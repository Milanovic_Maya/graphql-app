import React from 'react'
import PropTypes from 'prop-types'

import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const CounterView = ({ counter }) => (
    <>
        <div>
            Counter :{` ${counter}`}
        </div>
    </>
)

const GET_COUNTER = gql`
{
    counter @client
}
`;

const Counter = () => (
    <Query query={GET_COUNTER}>
        {({ data }) => <CounterView {...data} />}
    </Query>
);

CounterView.propTypes = {
    counter: PropTypes.number.isRequired
};

export default Counter;

