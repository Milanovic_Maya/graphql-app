import gql from 'graphql-tag';
import { PropTypes } from 'prop-types';
import React from 'react';
import { Mutation } from 'react-apollo';

const IncrementView = ({ onIncrement }) => (
    <button onClick={onIncrement}>+</button>
);

IncrementView.propTypes = {
    onIncrement: PropTypes.func.isRequired,
};

const INCREMENT_COUNTER = gql`
  mutation {
    incrementCounter @client
  }
`;

const Increment = () => (
    <Mutation mutation={INCREMENT_COUNTER}>
        {(incrementCounter, { client }) => <IncrementView onIncrement={() => {
            // works this way, but when resolver is called doesn't???
            
            console.log(incrementCounter()); //returns data as empty object

            const counterObj = client.readQuery({ query: gql`{counter @client}` })

            const { counter } = counterObj;

            return client.writeData({ data: { counter: counter + 1 } })
        }} />}
    </Mutation>
);
export default Increment;