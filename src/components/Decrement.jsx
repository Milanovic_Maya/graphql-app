import React from 'react';

import { Mutation } from 'react-apollo';

import PropTypes from 'prop-types';

import gql from 'graphql-tag';

// component
const DecrementView = ({ onDecrement }) => (
    <button onClick={onDecrement}>-</button>
);

DecrementView.propTypes = {
    onDecrement: PropTypes.func.isRequired
};

// mutation
const DECREMENT_COUNTER = gql`
    mutation{
        decrementCounter @client
    }
`;

// display component that mutates cache
const Decrement = () => (
    <Mutation mutation={DECREMENT_COUNTER}>
        {decrementCounter => <DecrementView onDecrement={decrementCounter} />}
    </Mutation>
);

export default Decrement;