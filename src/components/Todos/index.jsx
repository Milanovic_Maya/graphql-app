import React from 'react';

import PropTypes from 'prop-types';

import { Query } from 'react-apollo';

import gql from 'graphql-tag';

import Todo from './Todo';

// this component displays list of todos from cache

const TodosView = ({ todoList=[] }) => (
    <div>
        {todoList.map(todo => (
            <Todo
                completed={todo.completed}
                id={todo.id}
                key={todo.id}
                text={todo.text}
            />
        ))}
    </div>
);

TodosView.propTypes = {
    todos: PropTypes.array
};

// write query:

const GET_TODOS = gql`
{
    todos @client{
        id
        completed
        text
    }
}
`;

// display UI

const Todos = () => (
    <Query query={GET_TODOS}>
        {({ data: { todos } }) =>{
            return  <TodosView todoList={todos} />
        }}
    </Query>
);

export default Todos;