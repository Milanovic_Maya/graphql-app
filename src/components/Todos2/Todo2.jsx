import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';

const TodoView2 = ({ completed, title, onTodoClick }) => (
  <div
    onClick={onTodoClick}
    style={{ textDecoration: completed ? 'line-through' : 'none' }}
  >
    {title}
  </div>
);

//write which mutation  will be triggered on todo click

const TOGGLE_TODO = gql`
mutation ToggleTodo($id:String!){
  toggleTodo(id:$id) @client
}
`;


const Todo2 = ({ completed, id, title }) => (
  <Mutation mutation={TOGGLE_TODO} variables={{ id }}>
    {toggleTodo2 => <TodoView2
      onTodoClick={toggleTodo2}
      completed={completed}
      title={title}
    />}
  </Mutation>
)

TodoView2.propTypes = {
  completed: PropTypes.bool.isRequired,
  onTodoClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
}

export default Todo2;