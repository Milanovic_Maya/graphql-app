import gql from 'graphql-tag';

const query = gql`
{
    counter @client
}
`;

let nextTodoId = 0;

export default {
    Todo: {
        completed: () => false,
      },

    Mutation: {

        addTodo: (_, { text }, { cache }) => {

            // write query for complex, object type
            const query = gql`
            query GetTodos{
            todos @client{
                id
                text
                completed
            }
        }`

            const prevTodos = cache.readQuery({ query });

            const newTodo = {
                id: nextTodoId++,
                text,
                completed: false,
                // property used internally by Apollo Client to create the keys
                __typename: `Todo`,
            };

            const data = {
                todos: [...prevTodos.todos, newTodo]
            }

            cache.writeData({ data });

            return newTodo;
        },

        toggleTodo: (_, variables, { cache }) => {
            const id = `Todo:${variables.id}`;
            const fragment = gql`
              fragment completeTodo on Todo {
                completed
              }
            `;
            const todo = cache.readFragment({ fragment, id });

            const data = { ...todo, completed: !todo.completed };
            cache.writeData({ id, data });
            return null;
          },
        },

        decrementCounter: (_, params, { cache }) => {
            const { counter } = cache.readQuery({ query });
            const nextCounter = counter - 1;
            const data = {
              counter: nextCounter,
            };
            cache.writeData({ data });
            return nextCounter;
          },
          incrementCounter: (_, params, { cache }) => {
            const { counter } = cache.readQuery({ query });
            const nextCounter = counter + 1;
            const data = {
              counter: nextCounter,
            };
            cache.writeData({ data });
            return nextCounter;
          },
    }
