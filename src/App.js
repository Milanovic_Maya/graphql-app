import React, { Component } from 'react';

import AddTodo from './components/AddTodo';
import Counter from './components/Counter';
import Decrement from './components/Decrement';
import Increment from './components/Increment';
import Todos from './components/Todos';
import Todos2 from './components/Todos2/index';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        ...
        <h3>Counter</h3>
        <Counter />
        <Decrement />
        <Increment />
        <h3>TODOS</h3>
        <AddTodo />
        <Todos />
        <Todos2/>
        ...
      </div>
    );
  }
}

export default App;